# Fingers Out !

[![Youtube](https://img.youtube.com/vi/15rIJAZEMtI/0.jpg)](https://www.youtube.com/watch?v=15rIJAZEMtI)

_Ce projet, réalisé en à peine plus de 24h a pour but de faire perdurer l'initiative Fingers Out! de PV Nova._

_Il vient remplacer le tirage au sort en proposant un nouveau tirage tous les jours, semaines et mois._

## Et si je veux personnaliser mes contraintes?

L'outil permet, à partir d'un petit fichier de configuration, de pouvoir personnaliser les catégories, et les contraintes !

### D'accord, mais à quoi ca ressemble?

Pour bien comprendre, on va prendre un exemple simple : un menu.

``` yaml
ENTREES:                            # Identifiant de la catégorie, mais elle n'est pas visible...
    title: Pour commencer...        # Le nom de la catégorie qui sera affiché
    color: '#dd5454'                # Couleur du fond de la carte
    values:                         # La liste des valeurs possibles
        - Tartines de chèvre chaud
        - Tomates Mozza
        - Salade Caesar
        
VIANDES:
    title: On continue avec...
    color: 'rgb(84, 221, 157)'      # Un autre format possible pour les couleurs
    values:
        - Foie de veau
        - Escalope de dinde
        - Steak
        
LEGUMES:
    title: Accompagné de...
    color: '#d8dd54'
    values:
        - Frites
        - Ratatouille
        - Choux de bruxelle
        
DESSERTS:
    title: Et pour finir...
    color: '#ff0af6'
    values:
        - Profiterolles
        - Mousse au chocolat
        - Crumble aux pommes
```

### Super... mais après?

Et bien il suffit de le mettre en ligne ! Je recommande [pastebin](https://pastebin.com/) pour vos configurations.

### 1 - Écrire votre configuration dans pastebin

On rentre notre configuration dans 'New Paste'.

![Étape 1](https://i.imgur.com/UpXaG1L.png)

### 2 - Faire 'Create New Paste'

On clique sur 'Create New Paste'

![Étape 2](https://i.imgur.com/dwHopdD.png)

### 3 - Cliquer sur le bouton 'RAW'

Puis on veut le fichier brut en cliquant sur 'RAW'

![Étape 3](https://i.imgur.com/ExBR2Qx.png)

### 4 - Récupérer l'adresse du fichier

On copie l'URL dans la barre d'adresse, dans mon exemple : `https://pastebin.com/raw/gHFbKgfF`

![Étape 4](https://i.imgur.com/AqqQdT0.png)

### 5 - Et la donner à l'outil !

Et on la colle à la suite de :

`http://neivaalf.gitlab.io/fingers-out/#/?configUrl=`

ce qui donne

[http://neivaalf.gitlab.io/fingers-out/#/?configUrl=https://pastebin.com/raw/gHFbKgfF](http://neivaalf.gitlab.io/fingers-out/#/?configUrl=https://pastebin.com/raw/gHFbKgfF)

Et voilà !

![Étape 5](https://i.imgur.com/XMTdhmR.png)

## Tu peux intégrer 10 days stp ?

Et bien figures toi que le fichier de configuration est déjà prêt !

Et il est ici : [https://gitlab.com/Neivaalf/fingers-out/snippets/1707926/raw](https://gitlab.com/Neivaalf/fingers-out/snippets/1707926/raw)

Et donc si tu as suivi comment faire son fichier de configuration, le tirage de 10 days est disponible à cette adresse : [http://neivaalf.gitlab.io/fingers-out/#/?configUrl=https://gitlab.com/Neivaalf/fingers-out/snippets/1707926/raw](http://neivaalf.gitlab.io/fingers-out/#/?configUrl=https://gitlab.com/Neivaalf/fingers-out/snippets/1707926/raw)

## Je veux installer le projet !

C'est un projet basé sur la version webpack de VueJS.

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
